## Foreword

Hello!

This is an example student blog for the [2022-2023 ULB class "How To Make (almost) Any Experiments Using Digital Fabrication"](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/class-website/).

Each student has a personal GitLab remote repository in the [GitLab class group](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students) in which a template of a blog is already stored. 

The GitLab software  is set to use [MkDocs](https://www.mkdocs.org/) to turn simple text files written in [Markdown](https://en.wikipedia.org/wiki/Markdown) format, into the site you are navigating.

#### Edit your blog

There are several ways to edit your blog

* by navigating your remote GitLab repository and [editing the files using the GitLab Web Editor](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#edit-a-file).
* by using [git](https://rogerdudler.github.io/git-guide/) and Command Line on your computer : cloning a local copy of your online project repository, editing the files locally and synchronizing back your local repository with the copy on the remote server.

#### Publishing your blog

Two times a week and each time you change a file, the site is rebuilt and all the changes are published in few minutes.

#### It saves history

No worries, you can't break anything, all the changes you make are saved under [Version Control](https://en.wikipedia.org/wiki/Version_control) using [GIT](https://git-scm.com/book/en/v2/Getting-Started-About-Version-Control). This means that you have all the different versions of your page saved and available all the time in the Gitlab interface.

## About me

![](images/avatar-photo.jpg)

Hello world! My name is **Matthew Doyle**. I'm a third year biology student at the ULB.

Visit this website to learn more about me!

## My background

I was born in Braine-l'Alleud in Belgium. Although both my parents are Irish imigrants, I have lived in Belgium my entire life. I mostly grew up in Ixelles in Brussels, near the well known cimitière d'Ixelles. I have always been very interested in sports, and played football in a club from a very young age. I love travelling, and have visited many countries in Europe, as well as Ecuador and Honduras in Latin America. 

## Previous work

I have previously worked as a volunteer for [equilibrio Azul](https://www.equilibrioazul.org), an NGO which deals in marine wildlife conservation, particularly sea turtles. I also volunteered at the [Mooncoin residential care centre](https://www.seniorcare.ie/listings/mooncoin-residential-care-centre/), which is a nursing home specifically catered to the needs of people suffering from dementia.  

### Project A

This is an image from an external site:

![This is the image caption](https://images.unsplash.com/photo-1512436991641-6745cdb1723f?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=ad25f4eb5444edddb0c5fb252a7f1dce&auto=format&fit=crop&w=900&q=80)

While this is an image from the assets/images folder. Never use absolute paths (starting with /) when linking local images, always relative.

![This is another caption](images/sample-photo.jpg)
